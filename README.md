# Cost-based Clustering Analysis of Human Settlements

This R-package contains data and code to the to reproduce the results in the
*Cost-based clustering* paper [1]. 
If you don't have access to the journal paper, a preprint will be available at 
[Zenodo]() soon.

In the paper we analyse the spatial arrangement of human settlements. While
regular application of Ripley's *K*-function (or it's adjusted variant *L*) does
not provide significant evidence against Complete Spatial Randomness (CSR), 
using a Multi Dimensional Scaling approximation to the cost-based distances
among settlements reveal a strongly clustered arrangement.

This package contains two vignettes with all the code necessary to reproduce the 
results from the paper. You can browse the code 
[online](https://gitlab.com/famuvie/cbK/tree/dbb2792c29b85d0377fef3527fad95ce70ffce5c/inst/doc).

To browse offline or to play with the 
data yourself, install the package locally as follows from within an R session.

```r
## install dependencies
install.packages(
  c('devtools', 'dplyr', 'geoR', 'gdistance', 'ggplot2', 'raster', 'scales', 'spatstat', 'tidyr', 'viridis')
)
devtools::install_github('famuvie/geoRcb')

## download and install package cbK
if (.Platform$OS.type == "windows") setInternet2(TRUE)  # enable https download
tf <- tempfile()
download.file(
  "https://gitlab.com/famuvie/cbK/repository/archive.zip?ref=master",
  tf, mode = "wb"
)
unzip(tf, exdir = tempdir())
instdir <- list.files(tempdir(), pattern = "cbK-master", full.names = TRUE)
devtools::install(instdir, local = FALSE)

## Explore
browseVignettes("cbK")
```


[1] Submitted to Journal of Archaeological Science
